#!/usr/bin/env python3

# for emacs
# (run-python "python3")

import urllib.parse
import urllib.request
import datetime
import pickle
import time
from lxml import etree
from functools import reduce
from bs4 import BeautifulSoup


class OldMacrossNewsPage:
    def __init__(self, init_file):
        self.init_file = init_file
        self.news = self.get_old_data()

    def get_old_data(self):
        data_file = self.init_file
        return pickle.load(open(data_file, 'rb'))

    def get_news_by_id(self, id_to_search):
        for s_news in self.news:
            if s_news['link'] == id_to_search:
                return s_news
        return None


# should better make common class for the News Page and Description...
class MacrossNewsDescription:
    def __init__(self, link_url):
        self.url = link_url
        self.user_agent = ("Mozilla/5.0 (X11; Linux x86_64) " +
                           "AppleWebKit/537.36 (KHTML, like Gecko) " +
                           "Chrome/64.0.3282.119 Safari/537.36")
        self.page = self.get_page(self.url)

    def get_page(self, url):
        headers = {}
        headers['User-Agent'] = self.user_agent
        req = urllib.request.Request(url, None, headers)
        response = urllib.request.urlopen(req)
        html_doc = response.read()
        soup = BeautifulSoup(html_doc, 'html.parser')
        return soup

    def get_content_encoded_part(self):
        article_text_part = self.page.find_all(attrs={"class": "wysiwyg"})
        text_part = reduce(
            lambda curr_txt, more_txt: str(curr_txt) + str(more_txt),
            article_text_part[0].contents)
        text_part = text_part.replace('</img>', '')
        return text_part


class MacrossNewsPage:
    def __init__(self):
        self.url = 'http://macross.jp/news/'
        self.url_top = 'http://macross.jp/news/'
        self.user_agent = ("Mozilla/5.0 (X11; Linux x86_64) " +
                           "AppleWebKit/537.36 (KHTML, like Gecko) " +
                           "Chrome/64.0.3282.119 Safari/537.36")
        self.page = self.get_page(self.url)
        self.news = self.add_news()

    def get_page(self, url):
        headers = {}
        headers['User-Agent'] = self.user_agent
        req = urllib.request.Request(url, None, headers)
        response = urllib.request.urlopen(req)
        html_doc = response.read()
        soup = BeautifulSoup(html_doc, 'html.parser')
        return soup

    def add_news(self):
        page = self.page
        news = []
        # for item in page.div.article.find_all('li'):
        for item in page.find_all(name="a", attrs="post-list"):
            # curr_news = {}
            # curr_news['date'] = item.time['datetime']
            # curr_news['category'] = item.span.text
            # curr_news['header_news'] = item.a.text
            # curr_news['link'] = self.url_top + item.a['href'][2:]
            # news.append(curr_news)
            curr_news = {}
            curr_news['date'] = item.time.text.replace('.', '-')
            curr_news['category'] = item.find_all(
                'div',
                {'class': 'post-category-badge'})[0].text
            curr_news['header_news'] = item.p.text.strip()
            curr_news['link'] = item['href']
            news.append(curr_news)
        return news

    def get_news_by_id(self, id_to_search):
        for s_news in self.news:
            if s_news['link'] == id_to_search:
                return s_news
        return None

    def set_news_by_id(self, id_to_search, contents_dict):
        i = 0
        while i < len(news):
            if news[i]['link'] == id_to_search:
                news[i] = contents_dict
                return True
            i = i + 1
        raise NameError('IdNotFound')


class RssMacrossPage:
    def __init__(self, news):
        self.title_text = 'Macross news'
        self.link_text = 'http://macross.jp/news/'
        self.description_text = 'Macross news made into a pretty RSS, FIRE!!'
        self.language_text = 'ja'
        self.rss_text = self.make_complete_page()

    def date_converter(self, date_string):
        "converts from 2021-01-17 to an RSS readable date"
        year, month, day = date_string.split('-')
        date = datetime.date(int(year), int(month), int(day))
        new_date_string = date.strftime('%a, %d %b %Y %Z%H:%M:%S +0900')
        return new_date_string

    def make_complete_page(self):
        content_encoded_ns_string = 'http://purl.org/rss/1.0/modules/content/'
        c_e_ns_item_text = "{http://purl.org/rss/1.0/modules/content/}encoded"
        content_encoded_ns = {'content': content_encoded_ns_string}
        root = etree.Element('rss', version='2.0', nsmap=content_encoded_ns)
        channel = etree.SubElement(root, 'channel')
        title = etree.SubElement(channel, 'title')
        title.text = self.title_text
        link = etree.SubElement(channel, 'link')
        link.text = self.link_text
        description = etree.SubElement(channel, 'description')
        description.text = self.description_text
        language = etree.SubElement(channel, 'language')
        language.text = self.language_text
        for curr_news in news:
            # make elements
            item = etree.SubElement(channel, 'item')
            item_link = etree.SubElement(item, 'link')
            item_title = etree.SubElement(item, 'title')
            item_description = etree.SubElement(item, 'description')
            item_content_encoded = etree.SubElement(item, c_e_ns_item_text)
            # add contents to elements
            item_link.text = curr_news['link']
            item_title.text = ('{}|{}|{}'.format(
                curr_news['date'],
                curr_news['category'],
                curr_news['header_news']))
            # item_description.text = item_title.text
            item_description.text = item_title.text.replace(
                'src="/',
                'src="http://macross.jp/')
            item_content_encoded.text = etree.CDATA(
                curr_news['content_encoded'])
            item_pubdate = etree.SubElement(item, 'pubDate')
            item_pubdate.text = self.date_converter(curr_news['date'])
        return root


if __name__ == "__main__":
    already_fetched_data = 'old_news_raw'
    news_page = MacrossNewsPage()
    news = news_page.news
    prev_news = OldMacrossNewsPage(already_fetched_data)

    # include previously fetched data
    # by adding the 'content_encoded' key to the news dict
    # (you can skip if it's a new file)
    for curr_news in news:
        news_id = curr_news['link']
        new_news = news_page.get_news_by_id(news_id)
        old_news = prev_news.get_news_by_id(news_id)
        if old_news is not None and new_news != old_news:
            print("merging id = " + news_id)
            news_page.set_news_by_id(news_id, old_news)
        # news_page.set_news_by_id(news_id, new_news)
        else:
            print("no need to merge = " + news_id)

    # try to fetch a description for all news that don't have one
    for curr_news in news:
        if 'content_encoded' not in curr_news.keys():
            news_id = curr_news['link']
            curr_desc = MacrossNewsDescription(
                curr_news['link']).get_content_encoded_part()
            content_enc_dict = {'content_encoded': curr_desc}
            new_news = {**curr_news, **content_enc_dict}
            news_page.set_news_by_id(news_id, new_news)
            print('fetching ' + news_id)
            time.sleep(2)
        
    # save current data
    pickle.dump(news, open(already_fetched_data, 'wb'))

    # make an xmltree and dump it as a file
    # TODO add this to the class (maybe someday...)
    rss_obj = RssMacrossPage(news)
    rss_page = rss_obj.rss_text
    xmltree = etree.ElementTree(rss_page)
    xmltree.write('macross_news_rss.xml')
