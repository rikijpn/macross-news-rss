#!/usr/bin/env python3

# this was nice for a while
# but I ended up only seeing the non-simple version that has more contents
# so stopping this when they changed their page's format (around 2022/01)
import urllib.parse
import urllib.request
import datetime
from lxml import etree
from bs4 import BeautifulSoup


class MacrossNewsPage:
    def __init__(self):
        self.url = 'http://macross.jp/news/list.php'
        self.url_top = 'http://macross.jp/news/'
        self.user_agent = ( "Mozilla/5.0 (X11; Linux x86_64) " +
                            "AppleWebKit/537.36 (KHTML, like Gecko) " +
                            "Chrome/64.0.3282.119 Safari/537.36" )
        self.page = self.get_page()
        self.news = self.add_news()
    
    def get_page(self):
        headers = {}
        headers['User-Agent'] = self.user_agent
        req = urllib.request.Request(self.url, None, headers)
        response = urllib.request.urlopen(req)
        html_doc = response.read()
        soup = BeautifulSoup(html_doc, 'html.parser')
        return soup
    
    def add_news(self):
        page = self.page
        news = []
        for item in page.div.article.find_all('li'):
            curr_news = {}
            curr_news['date'] = item.time['datetime']
            curr_news['category'] = item.span.text
            curr_news['header_news'] = item.a.text
            curr_news['link'] = self.url_top + item.a['href'][2:]
            news.append(curr_news)
        return news
        

class RssMacrossPage:
    def __init__(self, news):
        self.title_text = 'Macross news'
        self.link_text = 'http://macross.jp/news/list.php'
        self.description_text = 'Macross news made into a pretty RSS, FIRE!!'
        self.language_text = 'ja'
        self.rss_text = self.make_complete_page()
    
    def date_convertor(self, date_string):
        "converts from 2018-03-06 to an RSS readable date"
        year, month, day = date_string.split('-')
        date = datetime.date(int(year), int(month), int(day))
        new_date_string = date.strftime('%a, %d %b %Y %Z%H:%M:%S +0900')
        return new_date_string
        
    def make_complete_page(self):
        root = etree.Element('rss', version = '2.0')
        channel = etree.SubElement(root, 'channel')
        title = etree.SubElement(channel, 'title')
        title.text = self.title_text
        link = etree.SubElement(channel, 'link')
        link.text = self.link_text
        description = etree.SubElement(channel, 'description')
        description.text = self.description_text
        language = etree.SubElement(channel, 'language')
        language.text = self.language_text
        for curr_news in news:
            item = etree.SubElement(channel, 'item')
            item_link = etree.SubElement(item, 'link')
            item_link.text = curr_news['link']
            item_description = etree.SubElement(item, 'description')
            item_title = etree.SubElement(item, 'title')
            item_title.text =  ( '{}|{}|{}'.format(
                curr_news['date'],
                curr_news['category'],
                curr_news['header_news']))
            item_description.text = item_title.text
            item_pubdate = etree.SubElement(item, 'pubDate')
            item_pubdate.text = self.date_convertor(curr_news['date'])
        return root


if __name__ == "__main__":
    news_page = MacrossNewsPage()
    news = news_page.news
    rss_obj = RssMacrossPage(news)
    rss_page = rss_obj.rss_text
    xmltree = etree.ElementTree(rss_page)
    xmltree.write('macross_news_rss_simple.xml')
